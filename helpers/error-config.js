const errorMessages = {
    internalError: 'Internal error - please contact administrator',
};

exports.errorMessages = errorMessages;
