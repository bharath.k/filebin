require('dotenv').config()
var MongoClient = require('mongodb').MongoClient;
var url = process.env.MONGO_URL;

let client = null;
const getMongoConnection = async () => {
    console.log("establishing mongo connection")
    try {
        client = await MongoClient.connect(url, { useUnifiedTopology: true });
    } catch (e) {
        console.log(e);
    }
    return client.db("filebin");
}

exports.getMongoConnection = getMongoConnection