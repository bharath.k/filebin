require('dotenv').config();
const fs = require('fs');
const AWS = require('aws-sdk');

const BUCKET_NAME = process.env.BUCKET_NAME ? process.env.BUCKET_NAME : "bharath-files";
const S3_KEY = process.env.KEY;
const S3_SECRET = process.env.SECRET;
const s3Secret = {
    accessKeyId: S3_KEY,
    secretAccessKey: S3_SECRET
};
const s3 = new AWS.S3(s3Secret);

const uploadFile = (files) => {
    try {
        const fileParams = {
            Bucket: BUCKET_NAME,
            Key: files.document.name,
            Body: files.document.data
        };
        return s3.upload(fileParams).promise();
    } catch (e) {
        console.log("upload error");
    }
};

const downloadFile = (res,fileInfo) => {
    try {
        res.set('Content-disposition', 'attachment; filename=' + fileInfo.filename);
        const params = {
            Bucket: BUCKET_NAME,
            Key: fileInfo.filename
        }
        s3.getObject(params)
                .createReadStream()
                .on('error', function (err) {
                    res.status(500).json({ error: "Error -> " + err });
                }).pipe(res);
    } catch (e) {
        console.log("upload error");
    }
};

exports.uploadFile = uploadFile;
exports.downloadFile = downloadFile;