const express = require('express');
const fileUpload = require('express-fileupload');
const morgan = require('morgan');
const app = express();
const port = process.env.PORT || 8000;

const healthRoute = require('./routes/health-routes')
const mainRoute = require('./routes/main-routes');

app.set('view engine', 'ejs')
app.use(morgan('dev'));

app.use(fileUpload({
    createParentPath: true,
    limits: { fileSize: 20 * 1048576 }
}));

app.use('/health',healthRoute);
app.use('/',mainRoute);

app.listen(port, () => {
    console.log(`app running on port:${port}`)
})