const healthController = {};

healthController.index = (req, res) => {
    return res.json({
        status: 'success',
        data: 'service is healthy'
    });
}

module.exports = healthController;