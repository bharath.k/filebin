const mongoHelper = require('../helpers/mongo-helper');
const fileHelper = require('../helpers/file-helper');
const { v4: uuidv4 } = require('uuid');
const ObjectID = require('mongodb').ObjectID;

const mainController = {};
mainController.index = (req, res) => {
    console.log('welcome to fileBin service');
    res.render('main', { view: "1" });
}

mainController.files = async (req, res, next) => {
    try {
        const mongoConnection = await mongoHelper.getMongoConnection();
        const collection = mongoConnection.collection('files');
        let allfiles = await collection.find({ loginId: req.params.loginId, isDownloaded: false }).toArray()
        res.render('main', { view: "2", allfiles });
    } catch (e) {
        console.error(e);
    }
}


mainController.uplaod = async (req, res, next) => {
    try {
        if (req.files) {
            const mongoConnection = await mongoHelper.getMongoConnection();
            const collection = mongoConnection.collection('files');
            let fileResponse = await fileHelper.uploadFile(req.files);
            let doc = {
                loginId: uuidv4(),
                filename: req.files.document.name,
                isDownloaded: false,
                status: true,
                s3res: fileResponse,
                created: new Date()
            }
            collection.insert(doc, { w: 1 })
                .then((doc) => {
                    res.redirect(`/filebin/${doc.ops[0].loginId}`)
                })
                .catch(error => {
                    console.error(error);
                    res.status(500).send({ status: "failure", message: error })
                });
        } else {
            res.status(400).send({ status: "failure", message: "Bad Request, No file given" })
        }
    } catch (e) {
        console.error(e);
    }
}

mainController.download = async (req, res, next) => {
    try {
        const mongoConnection = await mongoHelper.getMongoConnection();
        const collection = mongoConnection.collection('files');
        let fileInfo = await collection.findOne({ _id: ObjectID(req.params.fileId), isDownloaded: false });
        if (fileInfo) {
            collection.findOneAndUpdate({ _id: ObjectID(req.params.fileId) }, { $set: { isDownloaded: true, status: false } })
                .then((file) => {
                    fileHelper.downloadFile(res, file.value);
                })
                .catch(err => {
                    console.error(err);
                });
        } else {
            res.render('main', { view: "3" });
        }
    } catch (e) {
        console.error(e);
    }

}

module.exports = mainController;