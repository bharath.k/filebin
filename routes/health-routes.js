const express =require('express');

const HealthController = require('../controllers/health-controller');

const routes = express();

routes.get('',HealthController.index);

module.exports = routes;