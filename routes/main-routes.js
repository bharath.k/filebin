const express = require('express');

const mainController = require('../controllers/main-controller');

const routes = express();

routes.get('/', mainController.index);
routes.post('/upload', mainController.uplaod);
routes.get('/filebin/:loginId', mainController.files);
routes.get('/download/:fileId', mainController.download);

module.exports = routes;